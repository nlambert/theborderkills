// variables

var width = 1300;
var height = 900;

d3.select("svg")
    .attr("id","map")	
    .attr("preserveAspectRatio", "xMinYMin meet")
    .attr("class","back")
    .attr("viewBox", "0 0 " + width + " " + height)
    .style("background","#DDD");

var projection = d3.geoOrthographic()
.scale(1140)
.translate([width / 2 + 77 , height / 2 + 710])
.clipAngle(90)
.rotate([-20,0])
.precision(0.5);

var path = d3.geoPath().projection(projection); 


// Svg structure & layout

if(svg.selectAll(".sphere").empty()){
    
    
// Filters

var filter = svg.append("defs")
      .append("filter")
      .attr("id", "blur")
      .append("feGaussianBlur")
      .attr("stdDeviation", 10);    

var radialGradient = svg.append("defs")
       .append("radialGradient")
       .attr("id", "radial-gradient");

radialGradient.append("stop")
       .attr("offset", "20%")
       .attr("stop-color", "#4D769A");

radialGradient.append("stop")
       .attr("offset", "100%")
       .attr("stop-color", "#C0D6E7");

var tanaka = 1;
var defs = svg.append("defs");
var shadow = defs.append("filter")
       .attr("id", "drop-shadow")
       .attr("height", "130%");

shadow.append("feGaussianBlur")
       .attr("in", "SourceAlpha")
       .attr("stdDeviation", tanaka)
       .attr("result", "blur");
     
shadow.append("feOffset")
        .attr("in", "blur")
        .attr("dx", tanaka)
        .attr("dy", tanaka)
        .attr("result", "offsetBlur");

var feMerge = shadow.append("feMerge");
feMerge.append("feMergeNode").attr("in", "offsetBlur");
feMerge.append("feMergeNode").attr("in", "SourceGraphic");

var glow = defs.append("filter")
       .attr("id", "glow");

glow.append("feGaussianBlur")
        .attr("stdDeviation","2")
        .attr("result","glow");


// graticule
    
var graticule = d3.geoGraticule().step([10, 10]);

var sphere = svg.append("g")
      .attr("id","sphere")
	 .attr("class", "sphere");

var graticule = d3.geoGraticule().step([10, 10]);

var sphere1 = sphere.append("path")
        .datum(graticule.outline)
        .style("fill", "url(#radial-gradient)")
        .attr("class", "sphere")
        .attr("d", path)
        .attr("filter", "url(#blur)");

sphere.append("path")
        .datum(graticule)
        .attr("d", path);

// Countries

svg.append("g").attr("id","land");
svg.selectAll(".land").remove();
svg.selectAll("#land")
        .selectAll("path")
        .data(countries.features)
        .enter()
        .append("path")
        .attr("class","land")
        .attr("d", path);
        
// Schengen
svg.append("g").attr("id","schengen").style("filter", "url(#drop-shadow)");
svg.selectAll(".schengen").remove();
svg.selectAll("#schengen")
        .selectAll("path")
        .data(schengen.features)
        .enter()
        .append("path")
        .attr("class","schengen")
        .attr("d", path);
        
txt = "SCHENGEN";
x = 480;
y = 495;
svg.selectAll("#schengen").append("text").text(txt)
.attr("text-anchor", "middle")
.attr("x", x).attr("y", y)
.attr("transform", "translate(0,0) rotate(-18)")
.attr("class","txtshengen")
.style("filter", "url(#drop-shadow)");

// Circles
svg.append("g").attr("id","dots");    

// Study Area
svg.append("g").attr("id","bbox");  

// Study Area
svg.append("g").attr("id","legend");  

// Chart
svg.append("g").attr("id","chart");
svg.append("g").attr("id","chartvalues");

// Map Staging 

var layout = svg.append("g").attr("id","layout");

var layoutlabels = svg.append("g").attr("id","layoutlabels");

layout.append("text").text("Map Design: Nicolas LAMBERT, "+ r2d3.data[12])
.attr("text-anchor", "start").attr("x", 14).attr("y", 880).attr("class","txthelp");
layout.append("text").text("Data sources: United Against Racism (1993 - 1999), The Migrant's file (2000 - 2013), International Organization for Migration (2014 - 2020).")
.attr("text-anchor", "start").attr("x", 14).attr("y", 892).attr("class","txthelp");    

layout.append("rect")
    .attr("x", 0)
    .attr("y", 0)
    .attr("width", width)
    .attr("height", 60).
    attr("class","banner");

layout.append("rect")
    .attr("x", 0)
    .attr("y", height - 35)
    .attr("width", width)
    .attr("height", 35).
    attr("class","banner");    


layout.append("rect")
    .attr("x", 1075)
    .attr("y", 700)
    .attr("width", 225)
    .attr("height", 100).
    attr("class","bannerstat");
    
// Update

layout.append("text").text("Last update: " + r2d3.data[11])
.attr("text-anchor", "end").attr("x", 1290).attr("y", 887).attr("class","update");

// Labels ---------------------------------------------------------------------

txt = "Strait of Gibraltar";
x = 330;
y = 490;
layoutlabels.append("text").text(txt).attr("text-anchor", "middle")
.attr("x", x).attr("y", y).attr("class","txtlabelhalo").attr("filter", "url(#glow)");  
layoutlabels.append("text").text(txt).attr("text-anchor", "middle")
.attr("x", x).attr("y", y).attr("class","txtlabel");  

txt = "Strait of Sicily";
x = 600;
y = 470;
layoutlabels.append("text").text(txt).attr("text-anchor", "middle")
.attr("x", x).attr("y", y).attr("class","txtlabelhalo").attr("filter", "url(#glow)");  
layoutlabels.append("text").text(txt).attr("text-anchor", "middle")
.attr("x", x).attr("y", y).attr("class","txtlabel");  

txt = "Strait of Otronto";
x = 705;
y = 430;
layoutlabels.append("text").text(txt).attr("text-anchor", "middle")
.attr("x", x).attr("y", y).attr("class","txtlabelhalo").attr("filter", "url(#glow)");  
layoutlabels.append("text").text(txt).attr("text-anchor", "middle")
.attr("x", x).attr("y", y).attr("class","txtlabel");  

txt = "Lampedusa";
x = 600;
y = 500;
layoutlabels.append("text").text(txt).attr("text-anchor", "middle")
.attr("x", x).attr("y", y).attr("class","txtlabelhalo").attr("filter", "url(#glow)");  
layoutlabels.append("text").text(txt).attr("text-anchor", "middle")
.attr("x", x).attr("y", y).attr("class","txtlabel");  

txt = "Aegean Sea";
x = 805;
y = 460;
layoutlabels.append("text").text(txt).attr("text-anchor", "middle")
.attr("x", x).attr("y", y).attr("class","txtlabelhalo").attr("filter", "url(#glow)");  
layoutlabels.append("text").text(txt).attr("text-anchor", "middle")
.attr("x", x).attr("y", y).attr("class","txtlabel");  

txt = "Ceuta";
x = 322;
y = 506;
layoutlabels.append("text").text(txt).attr("text-anchor", "middle")
.attr("x", x).attr("y", y).attr("class","txtlabelhalo").attr("filter", "url(#glow)");  
layoutlabels.append("text").text(txt).attr("text-anchor", "middle")
.attr("x", x).attr("y", y).attr("class","txtlabel");  

txt = "Melilla";
x = 370;
y = 510;
layoutlabels.append("text").text(txt).attr("text-anchor", "middle")
.attr("x", x).attr("y", y).attr("class","txtlabelhalo").attr("filter", "url(#glow)");  
layoutlabels.append("text").text(txt).attr("text-anchor", "middle")
.attr("x", x).attr("y", y).attr("class","txtlabel");  

txt = "Canary Islands";
x = 148;
y = 623;
layoutlabels.append("text").text(txt).attr("text-anchor", "middle")
.attr("x", x).attr("y", y).attr("class","txtlabelhalo").attr("filter", "url(#glow)");  
layoutlabels.append("text").text(txt).attr("text-anchor", "middle")
.attr("x", x).attr("y", y).attr("class","txtlabel");  

txt = "Libyan Coasts";
x = 660;
y = 560; 
layoutlabels.append("text").text(txt).attr("text-anchor", "middle")
.attr("x", x).attr("y", y).attr("class","txtlabelhalo").attr("filter", "url(#glow)");  
layoutlabels.append("text").text(txt).attr("text-anchor", "middle")
.attr("x", x).attr("y", y).attr("class","txtlabel"); 

txt = "Sahara";
x = 640;
y = 740; 
layoutlabels.append("text").text(txt).attr("text-anchor", "middle")
.attr("x", x).attr("y", y).attr("class","txtlabelhalo").attr("filter", "url(#glow)");  
layoutlabels.append("text").text(txt).attr("text-anchor", "middle")
.attr("x", x).attr("y", y).attr("class","txtlabel");  

txt = "Calais";
x = 495;
y = 280; 
layoutlabels.append("text").text(txt).attr("text-anchor", "middle")
.attr("x", x).attr("y", y).attr("class","txtlabelhalo").attr("filter", "url(#glow)");  
layoutlabels.append("text").text(txt).attr("text-anchor", "middle")
.attr("x", x).attr("y", y).attr("class","txtlabel"); 

d3.select("#labels").on("change", function() { d3.selectAll("#layoutlabels").style("visibility", this.checked ? "visible" : "hidden"); });

// -------------------------------------------------------------

// Button export png

d3.select('.well').append("input")
  .attr("type", "button")
  .attr("name", "saveButton")
  .attr("id","saveButton")
  .attr("value", "Save your custom map as a png file");
  
}

// Circles

svg.selectAll(".dots").remove();
svg.selectAll("#dots").selectAll('path')
    .data(r2d3.data[0].features)
    .enter()
    .append("path")
    .attr("d", path)
    .attr("class","dots");
    
    
// Bbox

svg.selectAll(".bbox").remove();
svg.selectAll("#bbox").selectAll('path')
    .data(r2d3.data[1].features)
    .enter()
    .append("path")
    .attr("d", path)
    .attr("class","bbox");
    
// Map Title

svg.selectAll(".maptitle").remove();
svg.selectAll("#layout").append("text").text(r2d3.data[2])
.attr("x", 10).attr("y", 44)
.attr("class","maptitle");

// Texts

svg.selectAll(".legtitle").remove();

if(r2d3.data[10] === true){child = "Children";} else {child = "Migrants";}

svg.selectAll("#layout").append("text").text("Number of Dead and Missing")
.attr("text-anchor", "start").attr("x", 20).attr("y", 92).attr("class","legtitle");
svg.selectAll("#layout").append("text").text(child + " by year")
.attr("text-anchor", "start").attr("x", 20).attr("y", 112).attr("class","legtitle");

svg.selectAll("#layout").append("text").text("Number of Dead and Missing " + child)
.attr("text-anchor", "end").attr("x", 1280).attr("y", 92).attr("class","legtitle");
svg.selectAll("#layout").append("text").text("by Circles over the Time Period")
.attr("text-anchor", "end").attr("x", 1280).attr("y", 112).attr("class","legtitle");
    
svg.selectAll("#layout").append("text").text("The Fatal Policies")
.attr("text-anchor", "end").attr("x", 1290).attr("y", 645).attr("class","legtitle");
svg.selectAll("#layout").append("text").text(" of Fortress Europe ")
.attr("text-anchor", "end").attr("x", 1290).attr("y", 665).attr("class","legtitle");
svg.selectAll("#layout").append("text").text("in Figures")
.attr("text-anchor", "end").attr("x", 1290).attr("y", 685).attr("class","legtitle");

// Route

svg.selectAll(".route").remove();
svg.selectAll(".routerect").remove();

if (r2d3.data[9] != "All"){
var size = r2d3.data[9].length * 14;
svg.selectAll("#layout").append("rect")
    .attr("x", width/2 - size/2 - 10)
    .attr("y", 80)
    .attr("width", size + 20)
    .attr("height", 35)
    .attr("class","routerect");

svg.selectAll("#layout").append("text").text(r2d3.data[9])
.attr("x", width/2).attr("y", 105)
.attr("text-anchor", "middle")
.attr("class","route");
}



// Stats
var x = 1090 ;
var y = 725 ;
var delta = 15 ;
svg.selectAll(".stats").remove();

if(r2d3.data[4] !== 0){

svg.selectAll("#layout").append("text").text(r2d3.data[6][0] + " dead " + child + " by day")
.attr("x", x).attr("y", y)
.attr("class","stats");
svg.selectAll("#layout").append("text").text(r2d3.data[6][1] + " dead " + child + " by week")
.attr("x", x).attr("y", y + delta)
.attr("class","stats");
svg.selectAll("#layout").append("text").text(r2d3.data[6][2] + " dead " + child + " by month")
.attr("x", x).attr("y", y + delta * 2)
.attr("class","stats");
svg.selectAll("#layout").append("text").text(r2d3.data[6][3] + " dead " + child + " by year")
.attr("x", x).attr("y", y + delta * 3)
.attr("class","stats");
svg.selectAll("#layout").append("text").text(Math.round(r2d3.data[4] / r2d3.data[3] * 100) / 100 + " " + child + " by events")
.attr("x", x).attr("y", y + delta * 4)
.attr("class","stats");

}
// Chart

svg.selectAll(".chart").remove();
svg.selectAll(".txtchart").remove();
svg.selectAll(".txtchart2").remove();

if(r2d3.data[4] !== 0) {

var nbars = r2d3.data[5].nb.length;
var barheight = 24;
var x = 36; var y = 130; 

svg.selectAll('#chart').selectAll('rect')
  .data(r2d3.data[5].nb)
  .enter()
    .append('rect')
      .attr('height', barheight)
      .attr('width', function(d) { return d / 25; })
      .attr('y', function(d, i) { return y + i * barheight; })
      .attr('x', x)
      .attr('class', "chart");

svg.selectAll('#chart')
    .selectAll('text')
    .data(r2d3.data[5].year)
    .enter()
    .append('text')
    .text(function(d) { return d;})
    .attr('x', 5)
    .attr('y', function(d, i) { return y + i * barheight + barheight/1.5 })
    .attr('class', "txtchart");


svg.selectAll('#chartvalues')
    .selectAll('text')
    .data(r2d3.data[5].label)
    .enter()
    .append('text')
    .text(function(d) { return d;})
    .attr('x', function(d) { return x + d.split("(")[0] / 25 +3 ; })
    .attr('y', function(d, i) { return y + i * barheight + barheight/1.5 })
    .attr('class', "txtchart2");
    
}   


// Map Legend

svg.selectAll(".legend").remove();
svg.selectAll(".legendline").remove();
svg.selectAll(".txtlegend").remove();

k = 0.032;

x = width - Math.sqrt((r2d3.data[7][0] * k * r2d3.data[8])/3.14) - 70 ;
y = 130 + Math.sqrt((r2d3.data[7][0] * k * r2d3.data[8])/3.14)*2;

svg.selectAll('#legend')
    .selectAll('circle')
    .data(r2d3.data[7])
    .enter()
    .append('circle')
    .attr("cx", x)
    .attr("cy", function(d) { return y - Math.sqrt((d * k * r2d3.data[8])/3.14);})
    .attr("r",  function(d) { return  Math.sqrt((d * k * r2d3.data[8])/3.14) ; })
    .attr("class","legend");
    
svg.selectAll('#legend')
    .selectAll('line')
    .data(r2d3.data[7])
    .enter()
    .append('line')
    .attr("x1", x)
    .attr("y1", function(d) { return y - Math.sqrt((d * k * r2d3.data[8])/3.14) *2;})
    .attr("x2",  x + Math.sqrt((r2d3.data[7][0] * k * r2d3.data[8])/3.14) + 7 )
    .attr("y2", function(d) { return y - Math.sqrt((d * k * r2d3.data[8])/3.14)*2;})
    .attr("class","legendline");
    
    
svg.selectAll('#legend')
    .selectAll('text')
    .data(r2d3.data[7])
    .enter()
    .append('text')
    .text(function(d) { return d;})
    .attr("x",  x + Math.sqrt((r2d3.data[7][0] * k * r2d3.data[8])/3.14) + 10 )
    .attr("y", function(d) { return y - Math.sqrt((d * k * r2d3.data[8])/3.14) *2 + 3;})
    .attr("class","txtlegend");    

// ---------------------------------------------------------------------------
// Export PNG 
// ----------------------------------------------------------------------------


// Set-up the export button
d3.select('#saveButton').on('click', function(){
	var svgString = getSVGString(svg.node());
	svgString2Image( svgString, 2*width, 2*height, 'png', save ); // passes Blob and filesize String to the callback

	function save( dataBlob, filesize ){
		saveAs( dataBlob, 'TheBorderKills.png' ); // FileSaver.js function
	}
});

// Below are the functions that handle actual exporting:
// getSVGString ( svgNode ) and svgString2Image( svgString, width, height, format, callback )
function getSVGString( svgNode ) {
	svgNode.setAttribute('xlink', 'http://www.w3.org/1999/xlink');
	var cssStyleText = getCSSStyles( svgNode );
	appendCSS( cssStyleText, svgNode );

	var serializer = new XMLSerializer();
	var svgString = serializer.serializeToString(svgNode);
	svgString = svgString.replace(/(\w+)?:?xlink=/g, 'xmlns:xlink='); // Fix root xlink without namespace
	svgString = svgString.replace(/NS\d+:href/g, 'xlink:href'); // Safari NS namespace fix

	return svgString;

	function getCSSStyles( parentElement ) {
		var selectorTextArr = [];

		// Add Parent element Id and Classes to the list
		selectorTextArr.push( '#'+parentElement.id );
		for (var c = 0; c < parentElement.classList.length; c++)
				if ( !contains('.'+parentElement.classList[c], selectorTextArr) )
					selectorTextArr.push( '.'+parentElement.classList[c] );

		// Add Children element Ids and Classes to the list
		var nodes = parentElement.getElementsByTagName("*");
		for (var i = 0; i < nodes.length; i++) {
			var id = nodes[i].id;
			if ( !contains('#'+id, selectorTextArr) )
				selectorTextArr.push( '#'+id );

			var classes = nodes[i].classList;
			for (var c = 0; c < classes.length; c++)
				if ( !contains('.'+classes[c], selectorTextArr) )
					selectorTextArr.push( '.'+classes[c] );
		}

		// Extract CSS Rules
		var extractedCSSText = "";
		for (var i = 0; i < document.styleSheets.length; i++) {
			var s = document.styleSheets[i];
			
			try {
			    if(!s.cssRules) continue;
			} catch( e ) {
		    		if(e.name !== 'SecurityError') throw e; // for Firefox
		    		continue;
		    	}

			var cssRules = s.cssRules;
			for (var r = 0; r < cssRules.length; r++) {
				if ( contains( cssRules[r].selectorText, selectorTextArr ) )
					extractedCSSText += cssRules[r].cssText;
			}
		}
		

		return extractedCSSText;

		function contains(str,arr) {
			return arr.indexOf( str ) === -1 ? false : true;
		}

	}

	function appendCSS( cssText, element ) {
		var styleElement = document.createElement("style");
		styleElement.setAttribute("type","text/css"); 
		styleElement.innerHTML = cssText;
		var refNode = element.hasChildNodes() ? element.children[0] : null;
		element.insertBefore( styleElement, refNode );
	}
}


function svgString2Image( svgString, width, height, format, callback ) {
	var format = format ? format : 'png';

	var imgsrc = 'data:image/svg+xml;base64,'+ btoa( unescape( encodeURIComponent( svgString ) ) ); // Convert SVG string to data URL

	var canvas = document.createElement("canvas");
	var context = canvas.getContext("2d");

	canvas.width = width;
	canvas.height = height;

	var image = new Image();
	image.onload = function() {
		context.clearRect ( 0, 0, width, height );
		context.drawImage(image, 0, 0, width, height);

		canvas.toBlob( function(blob) {
			var filesize = Math.round( blob.length/1024 ) + ' KB';
			if ( callback ) callback( blob, filesize );
		});

		
	};

	image.src = imgsrc;
}
    


    

